/**
	 * \file
	 * \brief Jogo da Forca lit AF
	 * \details Enjoy the classic game in your terminal, written in Java. 
	 * \author Angelo Figueira
	 * \date 17/11/2017
	 * \bug No bugs whatsoever
	 * \version 1.0
	 * \copyright GNU Public License.
	 */
import java.util.Scanner;

public class Jogo {
	/** \brief Avalia se as letras sao repetidas ou nao existem na palavra
	*
	*/
	public static void main(String[] args) {
		System.out.println("Bem vindo ao jogo da forca");
		System.out.println("");
		String palavra[] = { "c", "h", "a", "v", "e" };  
		Scanner sc = new Scanner(System.in);  
		int forca = 1, vencer = 0, perder = 0, contador = 0;  
		
		String digito, letrasDigitadas[] = new String[20];  
		while (forca == 1) {  
		    int existePalavra = 0;  
		    System.out.println("Digite a letra:");  
		    digito = sc.next();  
		    letrasDigitadas[contador] = digito;
		    
		   
		    for (int i = 0; i < letrasDigitadas.length; i++) {  
		        if (letrasDigitadas[i] != null) {  
		            if (letrasDigitadas[i].equals(digito)) {  
		                existePalavra++;  
		            }  
		        }  
		    }  
		      
		    if (!(existePalavra > 1)) {  

		        for (int x = 0; x < palavra.length; x++) {  

		            if (palavra[x].equals(digito)) {  
		                System.out.println("Letra correta.");  
		                vencer++;  
		                break;  
		            } else {  
		                if (x == 4) {  
		                    System.out.println("Letra inexistente.");  
		                    perder++;  
		                }  
		            }  
		        }  
		        if (perder == 5) {  
		            System.out.println("Voc� perdeu.");  
		            System.exit(0);  
		        } else if (vencer == 5) {  
		            System.out.println("Voce venceu.");  
		            System.exit(0);  
		        }  
		    } else {  
		        System.out.println("Essa letra j� foi digitada.");  
		    }  
		    contador++;  
		}
		}
	}