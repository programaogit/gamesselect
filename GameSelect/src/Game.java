/**
     * \file
     * \brief mineSweeper - in Java
     * \details Enjoy the classic game in your terminal, written in Java. 
     * \author Angelo Figueira
     * \date 17/11/2017
     * \bug No bugs whatsoever
     * \version 1.0
     * \copyright GNU Public License.
     */

public class Game {
	
    private Board board; ///Using our previously defined Board object we initialise it here
    boolean finish = false;
    boolean win = false;
    int turn=0;
    /** \brief Inicializa-se o objeto Game e faz-se Play(board)
    */
    public Game(){
        board = new Board();
        Play(board);
        
    }
    /** \brief Misc methods and messages ingame 
    *   @param Board
    */
    public void Play(Board board){
        System.out.println("**********Bem vindo ao MineSweeper!**********");
        System.out.println("");
        do{
            turn++;
            System.out.println("Turno "+turn);
            board.show();
            finish = board.setPosition();
            
            if(!finish){
                board.openNeighbors();
                finish = board.win();
            }
            
        }while(!finish);
        
        if(board.win()){
            System.out.println("Parab�ns! Nao calhou em nenhuma Mina em "+turn+" turnos");
            board.showMines();
        } else {
            System.out.println("BOOM! Perdeu");
            board.showMines();
        }
    }
}