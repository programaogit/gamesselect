/**
	 * \file
	 * \brief Gerenciador do Jogo
	 * \details Desfrute da compilacao de jogos, feito em Java. 
	 * \author Angelo Figueira
	 * \date 17/11/2017
	 * \bug No bugs whatsoever
	 * \version 1.0
	 * \copyright GNU Public License.
	 */
public class Main {

    public static void main(String [] args){

        GerenciadordoJogo jogo = new GerenciadordoJogo(); ///chamamos a classe que escolhe os jogos

        jogo.iniciar();
        

    }

}