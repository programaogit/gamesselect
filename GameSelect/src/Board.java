/**
     * \file
     * \brief mineSweeper - mineSweeper board
     * \details Enjoy the classic game in your terminal, written in Java. 
     * \author Angelo Figueira
     * \date 17/11/2017
     * \bug No bugs whatsoever
     * \version 1.0
     * \copyright GNU Public License.
     */

import java.util.Random;
import java.util.Scanner;

public class Board {
    private int[][] mines;
    private char[][] boardgame;
    private int Line, Column;
    Random random = new Random();
    Scanner input = new Scanner(System.in);
    
    /** \brief declara um tabuleiro 10x10, atribui minas aleatorias a varias casas e imprime o tabuleiro
    */
    public Board (){
        mines = new int[10][10];
        boardgame = new char[10][10];
        startMines();
        randomMines();
        fillTips();
        startBoard();
        
    }
    /** \brief Avalia se o jogo foi ganho, fazendo recurso a um contador
    */
    public boolean win(){
        int count=0;
        for(int line = 1 ; line < 9 ; line++)
            for(int column = 1 ; column < 9 ; column++)
                if(boardgame[line][column]=='_')
                    count++;
        if(count == 10)
            return true;
        else
            return false;                
    }
    /** \brief Avalia se os slots adjacentes estao livres, mostrando-os se for o caso
    */
    public void openNeighbors(){
        for(int i=-1 ; i<2 ; i++)
            for(int j=-1 ; j<2 ; j++)
                if( (mines[Line+i][Column+j] != -1) && (Line != 0 && Line != 9 && Column != 0 && Column != 9) )
                    boardgame[Line+i][Column+j]=Character.forDigit(mines[Line+i][Column+j], 10);
        
    }
    /** \brief Obtem a posicao escolhida e transporta para o array
    *   @param int Line , int Column
    *   @return mines[Line][Column]
    */
    public int getPosition(int Line, int Column){
        return mines[Line][Column];
    }
    /** \brief Atribui a posicao ao valor correspondente
    */
    public boolean setPosition(){
            
            do{
                System.out.print("\nLinha: "); 
                Line = input.nextInt();
                System.out.print("Coluna: "); 
                Column = input.nextInt();
                
                if( (boardgame[Line][Column] != '_') && ((Line < 9 && Line > 0) && (Column < 9 && Column > 0)))
                    System.out.println("Campo j� esta preenchido");
                
                if( Line < 1 || Line > 8 || Column < 1 || Column > 8)
                    System.out.println("Escolha um numero entre 1 e 8");
                
            }while((Line < 1 || Line > 8 || Column < 1 || Column > 8) || (boardgame[Line][Column] != '_') );
            
            if(getPosition(Line, Column)== -1)
                return true;
            else
                return false;
            
    }
    /** \brief Metodo destinado a mostrar o tabuleiro com os seus valores
    */
    public void show(){
        System.out.println("\n     Linha");
        for(int Line = 8 ; Line > 0 ; Line--){
            System.out.print("       "+Line + " ");
            
            for(int Column = 1 ; Column < 9 ; Column++){
                    System.out.print("   "+ boardgame[Line][Column]);
            }
                
            System.out.println();
        }
            
        System.out.println("\n            1   2   3   4   5   6   7   8");
        System.out.println("                      Coluna");
        
    }
    /** \brief Preenche os slots
    */
    public void fillTips(){
        for(int line=1 ; line < 9 ; line++)
            for(int column=1 ; column < 9 ; column++){
                
                    for(int i=-1 ; i<=1 ; i++)
                        for(int j=-1 ; j<=1 ; j++)
                            if(mines[line][column] != -1)
                                if(mines[line+i][column+j] == -1)
                                    mines[line][column]++;
                
            }
            
    }
    /** \brief Marca com * as posicoes minadas
    */
    public void showMines(){
        for(int i=1 ; i < 9; i++)
            for(int j=1 ; j < 9 ; j++)
                if(mines[i][j] == -1)
                    boardgame[i][j]='*';
        
        show();
    }
    /** \brief Preenche o array boardgame
    */
    public void startBoard(){
        for(int i=1 ; i<mines.length ; i++)
            for(int j=1 ; j<mines.length ; j++)
                boardgame[i][j]= '_';
    }
    /** \brief To check the pumps around, we use two loops:
    *
    */
    public void startMines(){
        for(int i=0 ; i<mines.length ; i++)
            for(int j=0 ; j<mines.length ; j++)
                mines[i][j]=0;
    }
    /** \brief  Atribui minas em slots aleatorios
    */
    public void randomMines(){
        boolean raffled;
        int Line, Column;
        for(int i=0 ; i<10 ; i++){
            
            do{
                Line = random.nextInt(8) + 1;
                Column = random.nextInt(8) + 1;
                
                if(mines[Line][Column] == -1)
                    raffled=true;
                else
                    raffled = false;
            }while(raffled);
            
            mines[Line][Column] = -1;
        }
    }
}