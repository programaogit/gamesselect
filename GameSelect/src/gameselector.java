/**
	 * \file
	 * \brief Game Selector
	 * \details Desfrute dos jogos mais classicos no seu terminal.
	 * \author Angelo Figueira
	 * \date 17/11/2017
	 * \bug No bugs whatsoever
	 * \version 1.0
	 * \copyright GNU Public License.
	 */
import java.io.*;
import java.util.Scanner;
public class gameselector {
	private static Scanner input;
	public static void main(String[] args) throws IOException{

		while (true) {
		input = new Scanner(System.in);
		System.out.println("-------------------------------------------------");
		System.out.println("          Bem vindo ao gameSelector");
		System.out.println("");
		System.out.println("Pressiona 1 para jogar Jogo do Galo   ");
		System.out.println("");
		System.out.println("Pressiona 2 para jogar MineSweeper    ");
		System.out.println("");
		System.out.println("Pressiona 3 para jogar Jogo da Forca      ");
		System.out.println("");
		System.out.println("Pressiona 4 para sair                 ");
		System.out.println("-------------------------------------------------");
		String choice = input.next();
		if (choice.equalsIgnoreCase("1")) {
			Main.main(args);
		}
		if (choice.equalsIgnoreCase("2")) {
			MineSweeper.main(args);
		}
		
		if(choice.equalsIgnoreCase("3")) {
			Jogo.main(args);
			
		}

		if (choice.equalsIgnoreCase("4")) {
			System.exit(0);
		}
				
	}

	}
}